get pod details
==
$ kubectl get pods -n YOUR_NAMESPACE --output=wide
// will also print on which node a pod is located
$ kubectl describe pod YOUR_POD_NAME -n YOUR_NAMESPACE


get worker nodes CPU, MEMORY usage
==
$ kubectl get nodes -n YOUR_NAME_SPACE
$ kubectl describe node YOUR_K8S_NODE_NAME


show ingress instances
==
$ kubectl get ingress --show-labels --all-namespaces
$ kubectl get ing -n YOUR_NAMESACE
$ kubectl describe ing INGRESS_NAME -n YOUR_NAMESPACE
// Rules:
//  Host     | Path   | Backends
//  bla.com  | /pub/* | your-service:8080 (10.151.123.123:8080)


show service details
==
$ kubectl get ns
// will list all namespaces
$ kubectl get svc -n YOUR_NAMESPACE
// will list all services
$ kubectl describe svc YOUR_SERVICE_NAME -n YOUR_NAME_SPACE


create new kubectl config entry (new context)
==
$ gcloud container clusters get-credentials YOUR_CLUSTER_NAME_COULD_BE_SAME_AS_GCP_PROJECT --region australia-southeast1
// check for a new context in kubectl config
$ kubectl config get-contexts


rename context
==
$ kubectl config rename-context CONTEXT_NAME NEW_NAME


what kubectl is controlling currently?
which cluster and where?
==
$ kubectl config get-contexts
$ kubectl config current-context


change context/cluster
==
$ kubectl config get-contexts
$ kubectl config use-context CONTEXT_NAME


restart all services in the namespace
==
$ kubectl -n NAME_SPACE_NAME rollout restart deploy


create job locally & execute (kubectl from GCP or Rancher)
==
$ docker build . -t gcr.io/GCP-PROJECT-NAME/YOUR-POD-NAME
// check image was created
$ docker image ls -a | grep YOUR-POD-NAME

        DIDN'T WORK, USE ONE BELOW:$ kubectl create job job-YOUR-POD-NAME --image=gcr.io/GCP-PROJECT-NAME/YOUR-POD-NAME

$ kubectl create -f YOUR-POD-NAME.yml        
// check pod was created, copy full pod name (GUID ending)
$ kubectl get pods
// execute the job
$ kubectl logs YOUR-POD-NAME-GUID-ENDING


delete job
==
$ kubectl get pods
// copy name WITHOUT guid ending: job-bla-cfh2j becomes job-bla
$ kubectl delete job job-bla


k8s service types
==
ClusterIP (default)
PortNode
ExternalName
LoadBalancer (cloud only)


get external IP
==
kubectl describe nodes | grep ExternalIP


create service
==
kubectl expose deployment YOUR-DEPLOYMENT --type=ClusterIP --port 80


see services
==
$ kubectl get svc
$ kubectl get svc --all-namespaces


use local images created with "docker build ." on k8s (e.g. as a Job)
==
$ minikube docker-env
// see all commands needed to be run one by one, OR run this:
$ eval $(minikube -p minikube docker-env)

$ docker build . -t bla/hello-world
$ kubectl create -f helloworld.yml

// to see the result
$ kubectl get pods
$ kubectl logs hello-world-YOUR_VALUE_HERE
